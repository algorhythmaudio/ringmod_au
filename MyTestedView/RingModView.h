/************************************************************************************************************/
/* NOTE: It is important to rename ALL ui classes when using the XCode Audio Unit with Cocoa View template	*/
/*		 Cocoa has a flat namespace, and if you use the default filenames, it is possible that you will		*/
/*		 get a namespace collision with classes from the cocoa view of a previously loaded audio unit.		*/
/*		 We recommend that you use a unique prefix that includes the manufacturer name and unit name on		*/
/*		 all objective-C source files. You may use an underscore in your name, but please refrain from		*/
/*		 starting your class name with an undescore as these names are reserved for Apple.					*/
/*  Example  : AppleDemoFilter_UIView AppleDemoFilter_ViewFactory											*/
/************************************************************************************************************/


#import <Cocoa/Cocoa.h>
#import <AudioUnit/AudioUnit.h>
#import <AudioToolbox/AudioToolbox.h>

//#import "Knob.h"



typedef struct pic {
	CGFloat rotation;			// in degrees
    CGFloat min;
    CGFloat max;
} Pic;



@interface RingModView : NSView {
    double          valueFreq;
    double          valueDepth;
    int             currentType;
    
    /*~~~~~~~~~~~~~~~~ mouseClick ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     * - keeps track of where the user clicked on the interface
     * - value of -1 means no click
     * - value of kParam3Type's for the waveType
     * - value of 101 for knobFreq, 102 for knobDepth
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
    int                     mouseClick;
    CGFloat                 dragStart;
    
    NSImage                 *background;
    NSImage                 *dialFreq;
    NSImage                 *dialDepth;
    NSImage                 *knobFreq;
    NSImage                 *knobDepth;
    NSImage                 *sawDnOff;
    NSImage                 *sawDnOn;
    NSImage                 *sawUpOff;
    NSImage                 *sawUpOn;
    NSImage                 *sineOn;
    NSImage                 *sineOff;
    NSImage                 *squareOn;
    NSImage                 *squareOff;
    NSImage                 *triOn;
    NSImage                 *triOff;
    
    NSRect                  rectSine;
    NSRect                  rectSawUp;
    NSRect                  rectSawDn;
    NSRect                  rectTri;
    NSRect                  rectSquare;
    NSRect                  rectKnobFreq;
    NSRect                  rectKnobDepth;
    NSRect                  rectTextFreq;
    NSRect                  rectTextDepth;
    
    Pic                     *dialInfoFreq;
    Pic                     *dialInfoDepth;
    
    IBOutlet NSTextField    *textFreq;
    IBOutlet NSTextField    *textDepth;
    IBOutlet NSTextField    *labelFreq;
    IBOutlet NSTextField    *labelDepth;
       
@protected
    // other members
    AudioUnit               myAU;
    AUParameterListenerRef  mParamListener;
    AUEventListenerRef      mEventListener;
}

@property (nonatomic) double valueFreq, valueDepth;
@property (nonatomic) int currentType;


-(void) eventListener: (void*)inObject event: (const AudioUnitEvent *)inEvent value:(Float32)inValue;
-(void) setAU: (AudioUnit) inAU;

-(IBAction) textFieldFreq:(id)sender;
-(IBAction) textFieldDepth:(id)sender;

@end











