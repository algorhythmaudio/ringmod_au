//
//  MyTestedView.m
//  AuTesting
//
//  Created by GW Rodriguez on 6/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "RingModView.h"
#import "Parameters.h"

// add each new parameter to this list. Same as under Parameters.h
AudioUnitParameter parameterList[] = {  {0, kParam_1, kAudioUnitScope_Global, 0},
                                        {0, kParam_2, kAudioUnitScope_Global, 0},
                                        {0, kParam_3, kAudioUnitScope_Global, 0}};


void EventListenerDispatcher (void *inRefCon, void *inObject, const AudioUnitEvent *inEvent, UInt64 inHostTime, Float32 inValue) {
    RingModView *SELF = (RingModView *)inRefCon;
    [SELF eventListener:inObject event:inEvent value:inValue];
}


void addParamListener (AUEventListenerRef listener, void* refCon, AudioUnitEvent *inEvent) {
	inEvent->mEventType = kAudioUnitEvent_BeginParameterChangeGesture;
	verify_noerr ( AUEventListenerAddEventType(	listener, refCon, inEvent));
	
	inEvent->mEventType = kAudioUnitEvent_EndParameterChangeGesture;
	verify_noerr ( AUEventListenerAddEventType(	listener, refCon, inEvent));
	
	inEvent->mEventType = kAudioUnitEvent_ParameterValueChange;
	verify_noerr ( AUEventListenerAddEventType(	listener, refCon, inEvent));
}







@implementation RingModView

@synthesize valueFreq, valueDepth;
@synthesize currentType;



- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        // load images from the bundle to NSImage's
        NSBundle *bundle = [NSBundle bundleWithIdentifier:(NSString*) kBundleName];
        background = [[NSImage alloc] initWithContentsOfFile:[bundle pathForResource:@"RMBackground2" ofType:@"png"]];
        dialFreq = [[NSImage alloc] initWithContentsOfFile:[bundle pathForResource:@"RMDial" ofType:@"png"]];
        dialDepth = [[NSImage alloc] initWithContentsOfFile:[bundle pathForResource:@"RMDial" ofType:@"png"]];
        knobFreq = [[NSImage alloc] initWithContentsOfFile:[bundle pathForResource:@"RMKnob" ofType:@"png"]];
        knobDepth = [[NSImage alloc] initWithContentsOfFile:[bundle pathForResource:@"RMKnob" ofType:@"png"]];
        sawDnOff = [[NSImage alloc] initWithContentsOfFile:[bundle pathForResource:@"RMSawDnOff" ofType:@"png"]];
        sawDnOn = [[NSImage alloc] initWithContentsOfFile:[bundle pathForResource:@"RMSawDnOn" ofType:@"png"]];
        sawUpOff = [[NSImage alloc] initWithContentsOfFile:[bundle pathForResource:@"RMSawUpOff" ofType:@"png"]];
        sawUpOn = [[NSImage alloc] initWithContentsOfFile:[bundle pathForResource:@"RMSawUpOn" ofType:@"png"]];
        sineOff = [[NSImage alloc] initWithContentsOfFile:[bundle pathForResource:@"RMSineOff" ofType:@"png"]];
        sineOn = [[NSImage alloc] initWithContentsOfFile:[bundle pathForResource:@"RMSineOn" ofType:@"png"]];
        squareOff = [[NSImage alloc] initWithContentsOfFile:[bundle pathForResource:@"RMSquareOff" ofType:@"png"]];
        squareOn = [[NSImage alloc] initWithContentsOfFile:[bundle pathForResource:@"RMSquareOn" ofType:@"png"]];
        triOff = [[NSImage alloc] initWithContentsOfFile:[bundle pathForResource:@"RMTriOff" ofType:@"png"]];
        triOn = [[NSImage alloc] initWithContentsOfFile:[bundle pathForResource:@"RMTriOn2" ofType:@"png"]];
        
        currentType = kParam3TypeSine;
        
        // set mouse rects
        rectSine = NSMakeRect(11, 96, 68, 28);
        rectSawUp = NSMakeRect(91, 96, 68, 28);
        rectSawDn = NSMakeRect(173, 96, 68, 28);
        rectTri = NSMakeRect(250, 96, 68, 28);
        rectSquare = NSMakeRect(325, 96, 68, 28);
        rectKnobFreq = NSMakeRect(93, 32, 34, 34);
        rectKnobDepth = NSMakeRect(243, 32, 34, 34);
        rectTextFreq = NSMakeRect(87, 68, 58, 22);
        rectTextDepth = NSMakeRect(243, 68, 58, 22);

        // prep knob info
        dialInfoFreq = (Pic*) malloc(sizeof(Pic));
        if (dialInfoFreq) {
            dialInfoFreq->rotation = cbrtf(kParam1Default / kParam1Max);
            dialInfoFreq->min = 0.0;
            dialInfoFreq->max = 1.0;
        }
        
        dialInfoDepth = (Pic*) malloc(sizeof(Pic));
        if (dialInfoDepth) {
            dialInfoDepth->rotation = 0.5;
            dialInfoDepth->min = 0.0;
            dialInfoDepth->max = 1.0;
        }
        
        mouseClick = -1; // -1 means no selection
        [self setNeedsDisplay:YES];
    }
    
    return self;
}



#pragma mark - Drawing


- (NSImage *)rotateImage:(NSImage *)image angle:(float)alpha {
    int rotation = (int) -((alpha * 300) - 150);
    
    NSImage *existingImage = image;
    NSSize existingSize = [existingImage size];
    NSSize newSize = NSMakeSize(existingSize.height, existingSize.width);
    NSImage *rotatedImage = [[NSImage alloc] initWithSize:newSize];
    
    [rotatedImage lockFocus];
    [[NSGraphicsContext currentContext]
     setImageInterpolation:NSImageInterpolationNone];
    
    NSAffineTransform *rotateTF = [NSAffineTransform transform];
    NSPoint centerPoint = NSMakePoint(newSize.width / 2.0,
                                      newSize.height / 2.0);
    
    //translate the image to bring the rotation point to the center
    //(default is 0,0 ie lower left corner)
    
    [rotateTF translateXBy:centerPoint.x yBy:centerPoint.y];
    [rotateTF rotateByDegrees:rotation];
    [rotateTF translateXBy:-centerPoint.x yBy:-centerPoint.y];
    [rotateTF concat];
    
    [existingImage drawAtPoint:NSZeroPoint
                      fromRect:NSMakeRect(0, 0, newSize.width, newSize.height)
                     operation:NSCompositeSourceOver
                      fraction:1.0];
    
    [rotatedImage unlockFocus];
    
    return [rotatedImage autorelease];
}


- (void)drawRect:(NSRect)dirtyRect {
//    CGContextRef context = (CGContextRef) [[NSGraphicsContext currentContext] graphicsPort];
//    CGContextSaveGState(context);
    
    [NSGraphicsContext saveGraphicsState];
    
    // draw background
    [background drawAtPoint:NSZeroPoint fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1.0];
    
    // draw wave types
    switch (self.currentType) {
        case kParam3TypeSine:
            [sineOn drawAtPoint:NSMakePoint(0, 10) fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1.0];
            [sawUpOff drawAtPoint:NSMakePoint(0, 10) fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1.0];
            [sawDnOff drawAtPoint:NSMakePoint(0, 10) fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1.0];
            [triOff drawAtPoint:NSMakePoint(0, 10) fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1.0];
            [squareOff drawAtPoint:NSMakePoint(0, 10) fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1.0];
            break;
        case kParam3TypeSawUp:
            [sineOff drawAtPoint:NSMakePoint(0, 10) fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1.0];
            [sawUpOn drawAtPoint:NSMakePoint(0, 10) fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1.0];
            [sawDnOff drawAtPoint:NSMakePoint(0, 10) fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1.0];
            [triOff drawAtPoint:NSMakePoint(0, 10) fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1.0];
            [squareOff drawAtPoint:NSMakePoint(0, 10) fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1.0];
            break;
        case kParam3TypeSawDown:
            [sineOff drawAtPoint:NSMakePoint(0, 10) fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1.0];
            [sawUpOff drawAtPoint:NSMakePoint(0, 10) fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1.0];
            [sawDnOn drawAtPoint:NSMakePoint(0, 10) fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1.0];
            [triOff drawAtPoint:NSMakePoint(0, 10) fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1.0];
            [squareOff drawAtPoint:NSMakePoint(0, 10) fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1.0];
            break;
        case kParam3TypeTri:
            [sineOff drawAtPoint:NSMakePoint(0, 10) fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1.0];
            [sawUpOff drawAtPoint:NSMakePoint(0, 10) fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1.0];
            [sawDnOff drawAtPoint:NSMakePoint(0, 10) fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1.0];
            [triOn drawAtPoint:NSMakePoint(0, 10) fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1.0];
            [squareOff drawAtPoint:NSMakePoint(0, 10) fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1.0];
            break;
        case kParam3TypeSquare:
            [sineOff drawAtPoint:NSMakePoint(0, 10) fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1.0];
            [sawUpOff drawAtPoint:NSMakePoint(0, 10) fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1.0];
            [sawDnOff drawAtPoint:NSMakePoint(0, 10) fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1.0];
            [triOff drawAtPoint:NSMakePoint(0, 10) fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1.0];
            [squareOn drawAtPoint:NSMakePoint(0, 10) fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1.0];
            break;
    }
    
    // Draw knobs
    [knobFreq drawInRect:rectKnobFreq fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1.0];
    [knobDepth drawInRect:rectKnobDepth fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1.0];
    
    // draw dials
    NSImage *tempDial = [self rotateImage:dialFreq angle:dialInfoFreq->rotation];
    [tempDial drawInRect:rectKnobFreq fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1.0];
    
    tempDial = [self rotateImage:dialDepth angle:dialInfoDepth->rotation];
    [tempDial drawInRect:rectKnobDepth fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1.0];

    
    
    [NSGraphicsContext restoreGraphicsState];
}




#pragma mark - Event Listeners


// this is the method that the AU calls to send parameter data to the view
// add each parameter to the switch and update the views that interact with it.
//- (void)_parameterListener:(void *)inObject parameter:(const AudioUnitParameter *)inParameter value:(AudioUnitParameterValue)inValue {
//    // inObject ignored in this case.
//    
//	switch (inParameter->mParameterID) {
//		case kParam_1:
//            [textFreq setStringValue:[NSString stringWithFormat:@"%.2f", inValue]];
//            [labelFreq setStringValue:[NSString stringWithFormat:@"%.2f", inValue]];
//            break;
//        case kParam_2:
//            [textDepth setStringValue:[NSString stringWithFormat:@"%.2f", inValue]];
//            [labelDepth setStringValue:[NSString stringWithFormat:@"%.2f", inValue]];
//            break;
//        case kParam_3:
//            // do something
//            // shouldn't need to change the UI because it will have already been changed.
//            break;
//	}
//}
//

//void ParameterListenerDispatcher (void *inRefCon, void *inObject, const AudioUnitParameter *inParameter, AudioUnitParameterValue inValue) {
//	RingModView *SELF = (RingModView *)inObject;
//    [SELF _parameterListener:SELF parameter:inParameter value:inValue];
//}


-(void) _addListeners {
//	verify_noerr (AUListenerCreate (ParameterListenerDispatcher,
//                                    self,
//                                    CFRunLoopGetCurrent(),
//                                    kCFRunLoopDefaultMode,
//                                    0.100, // 100ms standard. This is min time allowed between calls
//                                    &mParamListener));
//	
//    for (int i = 0; i < kParamNum; i++) {
//        parameterList[i].mAudioUnit = myAU;
//        verify_noerr (AUListenerAddParameter(mParamListener, NULL, &parameterList[i]));
//    }
    
    verify_noerr(AUEventListenerCreate(EventListenerDispatcher,
                                       self,
                                       CFRunLoopGetCurrent(),
                                       kCFRunLoopDefaultMode,
                                       0.05,
                                       0.05,
                                       &mEventListener));
    
    AudioUnitEvent event;
    for (int i=0; i<kParamNum; i++) {
        parameterList[i].mAudioUnit = myAU;
        event.mArgument.mParameter = parameterList[i];
        
        addParamListener(mEventListener, self, &event);
    }
}


-(void) _removeListeners {
    for (int i = 0; i < kParamNum; i++)
        verify_noerr (AUListenerRemoveParameter(mParamListener, NULL, &parameterList[i]) );
    
	verify_noerr (AUListenerDispose(mParamListener) );
}


- (void)_synchronizeUIWithParameterValues {
    AudioUnitParameterValue value;
    
    for (int i=0; i<kParamNum; i++) {
        // first get the parameter id
        verify_noerr(AudioUnitGetParameter(myAU,
                                           parameterList[i].mParameterID,
                                           kAudioUnitScope_Global,
                                           0,
                                           &value));
        
        // this sets the parameter and notifies the listener
        verify_noerr (AUParameterSet (mParamListener,
                                      self,
                                      &parameterList[i],
                                      value,
                                      0));
        
        // this will notify the view of changes in the code (eg: presets)
        verify_noerr (AUParameterListenerNotify(mParamListener,
                                                self,
                                                &parameterList[i]));
    }
}


-(void) eventListener: (void*)inObject event: (const AudioUnitEvent *)inEvent value:(Float32)inValue {
    switch (inEvent->mEventType) {
        case kAudioUnitEvent_ParameterValueChange:
            switch (inEvent->mArgument.mParameter.mParameterID) {
                case kParam_1:
                    [textFreq setStringValue:[NSString stringWithFormat:@"%.2f", inValue]];
                    [labelFreq setStringValue:[NSString stringWithFormat:@"%.2f", inValue]];
                    break;
                case kParam_2:
                    [textDepth setStringValue:[NSString stringWithFormat:@"%.2f", inValue]];
                    [labelDepth setStringValue:[NSString stringWithFormat:@"%.2f", inValue]];
                    break;
                case kParam_3:
                    self.currentType = inValue;
                    [self setNeedsDisplay:YES];
                    break;
            }
            break;
        
        case kAudioUnitEvent_BeginParameterChangeGesture:
            break;
        
        case kAudioUnitEvent_EndParameterChangeGesture:
            
            break;
            
        case kAudioUnitEvent_PropertyChange:
            
            break;
    }
}



#pragma mark - Mouse


-(void) mouseDown:(NSEvent *)theEvent {
    [textFreq setHidden:YES];
    [textDepth setHidden:YES];
    
    NSPoint mouseLocation = [theEvent locationInWindow];
    NSPoint convertedLocation = [self convertPoint:mouseLocation fromView:nil];
    
    //************************ Wave Type *******************************
    // check for mouse click in a wave type box
    if (convertedLocation.y > rectSine.origin.y && convertedLocation.y < rectSine.origin.y+rectSine.size.height) {
        // check if in sine
        if (convertedLocation.x > rectSine.origin.x && convertedLocation.x < rectSine.origin.x + rectSine.size.width)
            mouseClick = kParam3TypeSine;
        
        // check if in saw up
        if (convertedLocation.x > rectSawUp.origin.x && convertedLocation.x < rectSawUp.origin.x + rectSawUp.size.width)
            mouseClick = kParam3TypeSawUp;
        
        // check if in saw down
        if (convertedLocation.x > rectSawDn.origin.x && convertedLocation.x < rectSawDn.origin.x + rectSawDn.size.width)
            mouseClick = kParam3TypeSawDown;
        
        // check if in triangle
        if (convertedLocation.x > rectTri.origin.x && convertedLocation.x < rectTri.origin.x + rectTri.size.width)
            mouseClick = kParam3TypeTri;
        
        // check if in square
        if (convertedLocation.x > rectSquare.origin.x && convertedLocation.x < rectSquare.origin.x + rectSquare.size.width)
            mouseClick = kParam3TypeSquare;
    }
    
    //********************** Knobs **********************************
    // check for click within y coordinates
    if (convertedLocation.y > rectKnobFreq.origin.y && convertedLocation.y < rectKnobFreq.origin.y + rectKnobFreq.size.height) {
        // check if it's the freq knob
        if (convertedLocation.x > rectKnobFreq.origin.x && convertedLocation.x < rectKnobFreq.origin.x + rectKnobFreq.size.width) {
            mouseClick = 101;
            dragStart = convertedLocation.y;
        }

        // check if it's the depth knob
        if (convertedLocation.x > rectKnobDepth.origin.x && convertedLocation.x < rectKnobDepth.origin.x + rectKnobDepth.size.width) {
            mouseClick = 102;
            dragStart = convertedLocation.y;
        }
    }

    //********************* Text Fields ********************************
    // first check for a double click
    if ([theEvent clickCount] >= 2) {
        // check for click within y coordintes
        if (convertedLocation.y > rectTextFreq.origin.y && convertedLocation.y < rectTextFreq.origin.y + rectTextFreq.size.height) {
            // check if it's the freq text field
            if (convertedLocation.x > rectTextFreq.origin.x && convertedLocation.x < rectTextFreq.origin.x + rectTextFreq.size.width)
                [textFreq setHidden:NO];
            
            // check if it's the depth text field
            if (convertedLocation.x > rectTextDepth.origin.x && convertedLocation.x < rectTextDepth.origin.x + rectTextDepth.size.width)
                [textDepth setHidden:NO];
        }
    }
}


-(void) mouseDragged:(NSEvent *)theEvent {
    if (mouseClick != 101 && mouseClick != 102) return;
    
    NSPoint mouseLocation = [theEvent locationInWindow];
    NSPoint convertedLocation = [self convertPoint:mouseLocation fromView:nil];
    
    CGFloat difference = convertedLocation.y - dragStart;
    
    switch (mouseClick) {
        case 101:
            if ([theEvent modifierFlags] & NSCommandKeyMask) // command key
                dialInfoFreq->rotation += difference * 0.001;
            else
                dialInfoFreq->rotation += difference * 0.005;
            
            // check limits
            if (dialInfoFreq->rotation >= dialInfoFreq->max)
                dialInfoFreq->rotation = dialInfoFreq->max;
            if (dialInfoFreq->rotation <= dialInfoFreq->min)
                dialInfoFreq->rotation = dialInfoFreq->min;
            
            // set the dialVal
            self.valueFreq = (powf(dialInfoFreq->rotation, 3.0)) * kParam1Max;
            break;
        case 102:
            if ([theEvent modifierFlags] & NSCommandKeyMask) // command key
                dialInfoDepth->rotation += difference * 0.001;
            else
                dialInfoDepth->rotation += difference * 0.005;
            
            // check limits
            if (dialInfoDepth->rotation >= dialInfoDepth->max)
                dialInfoDepth->rotation = dialInfoDepth->max;
            if (dialInfoDepth->rotation <= dialInfoDepth->min)
                dialInfoDepth->rotation = dialInfoDepth->min;
            
            // set the dialVal
            self.valueDepth = dialInfoDepth->rotation * 100.0;
            break;
    }
    
    dragStart = convertedLocation.y;
    
   [self setNeedsDisplay:YES];
}


-(void) mouseUp:(NSEvent *)theEvent {
    if (mouseClick == -1) return;
    
    NSPoint mouseLocation = [theEvent locationInWindow];
    NSPoint convertedLocation = [self convertPoint:mouseLocation fromView:nil];
    
    // first check that mouse is in y axis
    if (convertedLocation.y > rectSine.origin.y && convertedLocation.y < rectSine.origin.y+rectSine.size.height) {
        
        switch (mouseClick) {
            case kParam3TypeSine:
                if (convertedLocation.x > rectSine.origin.x && convertedLocation.x < rectSine.origin.x + rectSine.size.width)
                    self.currentType = kParam3TypeSine;
                break;
            case kParam3TypeSawUp:
                if (convertedLocation.x > rectSawUp.origin.x && convertedLocation.x < rectSawUp.origin.x + rectSawUp.size.width)
                    self.currentType = kParam3TypeSawUp;
                break;
            case kParam3TypeSawDown:
                if (convertedLocation.x > rectSawDn.origin.x && convertedLocation.x < rectSawDn.origin.x + rectSawDn.size.width)
                    self.currentType = kParam3TypeSawDown;
                break;
            case kParam3TypeTri:
                if (convertedLocation.x > rectTri.origin.x && convertedLocation.x < rectTri.origin.x + rectTri.size.width)
                    self.currentType = kParam3TypeTri;
                break;
            case kParam3TypeSquare:
                if (convertedLocation.x > rectSquare.origin.x && convertedLocation.x < rectSquare.origin.x + rectSquare.size.width)
                    self.currentType = kParam3TypeSquare;
                break;
        }
    }
    mouseClick = -1;
    [self setNeedsDisplay:YES];
}









#pragma mark - Public


-(void) setAU: (AudioUnit) inAU {
    NSLog(@"setAU called");
    if (myAU) [self _removeListeners];
    
    myAU = inAU;
    
    if (myAU) {
        NSLog(@"inAU passed");
        [self _addListeners];
        [self _synchronizeUIWithParameterValues];
        
        self.valueFreq = kParam1Default;
        self.valueDepth = kParam2Default;
    }
    else
        NSLog(@"couldn't sync parameters with view");
}


#pragma mark View Setters


-(void) setValueFreq:(double) freq {
    if (freq < kParam1Min) freq = kParam1Min;
    if (freq > kParam1Max) freq = kParam1Max;
    
    valueFreq = freq;
    
    [textFreq setStringValue:[NSString stringWithFormat:@"%.2f", freq]];
    [labelFreq setStringValue:[NSString stringWithFormat:@"%.2f", freq]];
    
    // other code to update AU
    verify_noerr(AUParameterSet(mParamListener,
                                NULL,
                                &parameterList[0], // kParam1 is frequency
                                (Float32) freq,
                                0));
}


-(void) setValueDepth:(double) depth {
    if (depth < kParam2Min) depth = kParam2Min;
    if (depth > kParam2Max) depth = kParam2Max;
    
    valueDepth = depth;
    
    [textDepth setStringValue:[NSString stringWithFormat:@"%.2f", depth]];
    [labelDepth setStringValue:[NSString stringWithFormat:@"%.2f", depth]];
    
    // other code to update AU
    verify_noerr(AUParameterSet(mParamListener,
                                NULL,
                                &parameterList[1], // kParam2 is depth
                                (Float32) depth,
                                0));
}


-(void) setCurrentType:(int) type {
    currentType = type;
    
    // other code to update AU
    verify_noerr(AUParameterSet(mParamListener,
                                NULL,
                                &parameterList[2], // kParam3 is waveType
                                type,
                                0));
}






#pragma mark IBAction



-(IBAction) textFieldFreq:(id)sender {
    self.valueFreq = [sender floatValue];
    
    [labelFreq setStringValue:[NSString stringWithFormat:@"%.2f", self.valueFreq]];
    dialInfoFreq->rotation = cbrtf(self.valueFreq / kParam1Max);
    
    [textFreq setHidden:YES];
    [self setNeedsDisplay:YES];
}


-(IBAction) textFieldDepth:(id)sender {    
    self.valueDepth = [sender floatValue];

    [labelDepth setStringValue:[NSString stringWithFormat:@"%.2f", self.valueDepth]];
    dialInfoDepth->rotation = self.valueDepth / 100.0;

    [textDepth setHidden:YES];
    [self setNeedsDisplay:YES];
}


@end

















