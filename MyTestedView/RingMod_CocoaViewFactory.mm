
#import "RingMod_CocoaViewFactory.h"
#import "RingModView.h"

@implementation RingMod_CocoaViewFactory

// version 0
- (unsigned) interfaceVersion {
	return 0;
}

// string description of the Cocoa UI
- (NSString *) description {
	return @"RingMod View";
}

// N.B.: this class is simply a view-factory,
// returning a new autoreleased view each time it's called.
- (NSView *)uiViewForAudioUnit:(AudioUnit)inAU withSize:(NSSize)inPreferredSize {
    NSLog(@"checking for xib");
    if (! [NSBundle loadNibNamed: @"RingModView" owner:self]) {
        NSLog (@"Unable to load nib for view.");
		return nil;
	}
    else {
        NSLog(@"found xib");

        // This particular nib has a fixed size, so we don't do anything with the inPreferredSize argument.
        // It's up to the host application to handle.
        [uiView setAU:inAU];
        
        NSView *returnView = uiView;
        uiView = nil;	// zero out pointer.  This is a view factory.  Once a view's been created
                                    // and handed off, the factory keeps no record of it.
        
        return [returnView autorelease];
    }
}

@end
