//
//  TextureView.h
//  RingMod
//
//  Created by GW on 12/9/12.
//
//

#import <Cocoa/Cocoa.h>

@interface TextureView : NSView {
    NSImage     *texture;
}

@end
