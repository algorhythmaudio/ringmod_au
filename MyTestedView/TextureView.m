//
//  TextureView.m
//  RingMod
//
//  Created by GW on 12/9/12.
//
//

#import "TextureView.h"

@implementation TextureView

- (id)initWithFrame:(NSRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        NSBundle *bundle = [NSBundle bundleWithIdentifier:(NSString*) kBundleName];
        texture = [[NSImage alloc] initWithContentsOfFile:[bundle pathForResource:@"RMTexture" ofType:@"png"]];
        
        [self setNeedsDisplay:YES];
    }
    
    return self;
}

- (void)drawRect:(NSRect)dirtyRect {
    [texture drawAtPoint:NSZeroPoint fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1.0];
}

@end
