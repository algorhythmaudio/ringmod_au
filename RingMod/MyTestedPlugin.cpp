#include "MyTestedPlugin.h"


#include "stdio.h"
#include "string.h"


#define kResampleFactor 4
#define kHarmsSquare 6
#define kHarmsSaw 6
#define kHarmsTri 10



#pragma mark Create and Destroy

// this is the constructor method.  this is where all the init happens
MyTestedPluginKernal::MyTestedPluginKernal(AUEffectBase *inAudioUnit) : AUKernelBase(inAudioUnit) {
    sr = GetSampleRate();
    UInt32 frameSize = inAudioUnit->GetMaxFramesPerSlice();
    
    osc = new AAOsc(kTableSize, sr*kResampleFactor);
    if (osc == NULL) {
        puts("|\tno memory for osc");
        throw CAException(kAudioUnitErr_FailedInitialization);
    }
    
    osc->initOsc(NULL, 0, kHarmsSaw, kHarmsSaw, kHarmsTri, kHarmsSquare);
    osc->initSigVector(frameSize*kResampleFactor);
    
    // init resample ivars
    sampleUp.initialize(sr, kResampleFactor, resampleQuality_Good);
    sampleDn.initialize(sr*kResampleFactor, kResampleFactor, resampleQuality_Good);
    
    // init buffers
    buffUpSampleIn = (Float32*) calloc(frameSize*kResampleFactor, sizeof(Float32));
    if (buffUpSampleIn == NULL) {
        puts("|\tError: unable to alloc memory for buffUpSampleIn");
        throw CAException(kAudioUnitErr_FailedInitialization);
    }
    
    buffUpSampleOut = (Float32*) calloc(frameSize*kResampleFactor, sizeof(Float32));
    if (buffUpSampleOut == NULL) {
        puts("|\tError: unable to alloc memory for buffUpSampleOut");
        throw CAException(kAudioUnitErr_FailedInitialization);
    }
    
    oscBuff = (Float32*) calloc(frameSize*kResampleFactor, sizeof(Float32));
    if (oscBuff == NULL) {
        puts("|\tError: unable to alloc memory for oscBuff");
        throw CAException(kAudioUnitErr_FailedInitialization);
    }
}


MyTestedPluginKernal::~MyTestedPluginKernal() {
    if (osc) delete osc;
    if (buffUpSampleIn) free(buffUpSampleIn);
    if (buffUpSampleOut) free(buffUpSampleOut);
    if (oscBuff) free(oscBuff);
}


// here you define all the parameters that will be use
MyTestedPlugin::MyTestedPlugin(AudioUnit component) : AUEffectBase(component) {
    CreateElements();
    Globals() -> UseIndexedParameters(kParamNum);
    
    SetParameter(kParam_1, kParam1Default);
    SetParameter(kParam_2, kParam2Default);
    SetParameter(kParam_3, kParam3Default);
    
    // this sets the factory preset
    SetAFactoryPresetAsCurrent(kPresets[kPresetDefault]);
}


// this can update the view if uninitialized (see FilterDemo)
OSStatus MyTestedPlugin::Initialize() {
    OSStatus result = AUEffectBase::Initialize();
    
    if (result == noErr) {
//        puts("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
//        puts("DRM start");
//        
//        FILE *tf = NULL;
//        int a97 = 0;
//        
//        tf = fopen("/Library/Audio/Plug-Ins/Components/RingMod.component/Contents/Resources/algo97", "rb");
//        //tf = fopen("~/Library/Audio/Plug-Ins/Components/RingMod.component/Contents/Resources/algo97", "rb");
//        
//        if (!tf) {
//            puts("Error: couldn't open drm");
//            return kAudioUnitErr_Unauthorized;
//        }
//        
//        fseek(tf, 0, SEEK_END);
//        unsigned long fileLen = ftell(tf);
//        fseek(tf, 0, SEEK_SET);
//        
//        char* buffer = (char*) malloc(fileLen+1);
//        if (!buffer) {
//            puts("Error: couldn't malloc drm");
//            return kAudioUnitErr_Unauthorized;
//        }
//        
//        // store into buffer
//        fread(buffer, fileLen, 1, tf);
//        
//        // check it!
//        puts("checking");
//        const char cmp[9] = {"RING_MOD"};
//        
//        for (int i=0; i<fileLen; i++) {
//            if (cmp[i] != buffer[i])
//                a97--;
//        }
//        
//        if (a97 != 0) {
//            printf("invalid: %i\n", a97);
//            return kAudioUnitErr_Unauthorized;
//        }
        
    }
    return result;
}


void MyTestedPluginKernal::Reset() {
    osc->setPhase(0.0);
}




#pragma mark - Getters - Parameters

// this function sets different parameters for each parameter
OSStatus MyTestedPlugin::GetParameterInfo(AudioUnitScope			inScope,
                                          AudioUnitParameterID      inParameterID,
                                          AudioUnitParameterInfo	&outParameterInfo ) 
{
    ComponentResult result = noErr;
    
    // tells the host that all the parameters are readable and writable
    outParameterInfo.flags = kAudioUnitParameterFlag_IsReadable | kAudioUnitParameterFlag_IsWritable;
    
    if (inScope == kAudioUnitScope_Global) {
        switch (inParameterID) {
            case kParam_1:
                AUBase::FillInParameterName(outParameterInfo, kParam1Name, false);
                outParameterInfo.unit = kAudioUnitParameterUnit_Hertz;
                outParameterInfo.minValue = kParam1Min;
                outParameterInfo.maxValue = kParam1Max;
                outParameterInfo.defaultValue = kParam1Default;
                break;
            case kParam_2:
                AUBase::FillInParameterName(outParameterInfo, kParam2Name, false);
                outParameterInfo.unit = kAudioUnitParameterUnit_Percent;
                outParameterInfo.minValue = kParam2Min;
                outParameterInfo.maxValue = kParam2Max;
                outParameterInfo.defaultValue = kParam2Default;
                break;
            case kParam_3:
                AUBase::FillInParameterName(outParameterInfo, kParam3Name, false);
                outParameterInfo.unit = kAudioUnitParameterUnit_Indexed;
                outParameterInfo.minValue = kParam3Min;
                outParameterInfo.maxValue = kParam3Max;
                outParameterInfo.defaultValue = kParam3Default;
                break;
            default:
                result = kAudioUnitErr_InvalidParameter;
                break;
        }
    }
    else result = kAudioUnitErr_InvalidParameter;
    
    return result;
}


// this method is so the au can get the names of each of the indexed values
OSStatus MyTestedPlugin::GetParameterValueStrings(AudioUnitScope		inScope,
                                                  AudioUnitParameterID	inParameterID,
                                                  CFArrayRef *			outStrings) 
{
    if ((inScope == kAudioUnitScope_Global) && (inParameterID == kParam_3)) {
        if (outStrings == NULL) return noErr;
        
        CFStringRef strings[] = {kParam3TypeSineName, kParam3TypeSawUpName, kParam3TypeSawDownName, kParam3TypeTriName, kParam3TypeSquareName};
        *outStrings = CFArrayCreate(NULL, (const void**) strings, kParam3TypeNum, NULL);
        return noErr;
    }
    return  kAudioUnitErr_InvalidProperty;
}




#pragma mark Properties


OSStatus MyTestedPlugin::GetPropertyInfo(AudioUnitPropertyID    inID,
                                         AudioUnitScope			inScope,
                                         AudioUnitElement		inElement,
                                         UInt32                 &outDataSize,
                                         Boolean                &outWritable )
{
    if (inScope == kAudioUnitScope_Global) {
        switch (inID) {
            case kAudioUnitProperty_CocoaUI:
                if (kHasCustomView) {
                    puts("GetPropertyInfo call and ready for cocoa view");
                    outWritable = false;
                    outDataSize = sizeof(AudioUnitCocoaViewInfo);
                    return noErr;
                }
        }
    }
    return AUEffectBase::GetPropertyInfo(inID, inScope, inElement, outDataSize, outWritable);
}


OSStatus MyTestedPlugin::GetProperty(AudioUnitPropertyID 	inID,
                                     AudioUnitScope 		inScope,
                                     AudioUnitElement 		inElement,
                                     void 					*outData )
{
    if (inScope == kAudioUnitScope_Global) {
        switch (inID) {
            case kAudioUnitProperty_CocoaUI: // allows host to find the custom view
                if (kHasCustomView) {
                    CFBundleRef bundle = CFBundleGetBundleWithIdentifier(kBundleName);
                    if (bundle == NULL) return fnfErr;
                    
                    CFURLRef bundleURL = CFBundleCopyResourceURL(bundle, kViewName, CFSTR("bundle"), NULL);
                    if (bundleURL == NULL) return fnfErr;
                    
                    CFStringRef className = kViewClass;
                    AudioUnitCocoaViewInfo cocoaInfo = {bundleURL, className};
                    *((AudioUnitCocoaViewInfo*) outData) = cocoaInfo;
                    
                    return noErr;
                }
        }
    }
    // if we've gotten this far, handles the standard properties
	return AUEffectBase::GetProperty (inID, inScope, inElement, outData);
}




#pragma mark Presets

// this function prepares the presets.  No need to touch this
OSStatus MyTestedPlugin::GetPresets (CFArrayRef *outData) const {
    if (outData == NULL) return noErr;
    
    CFMutableArrayRef presetsArray = CFArrayCreateMutable(NULL, kPresetNum, NULL);
    
    for (int i=0; i<kPresetNum; i++)
        CFArrayAppendValue(presetsArray, &kPresets[i]);
    
    *outData = (CFArrayRef) presetsArray;
    return noErr;
}







#pragma mark - Utility

OSStatus MyTestedPlugin::NewFactoryPresetSet (const AUPreset &inNewFactoryPreset) {
    SInt32 choosenPreset = inNewFactoryPreset.presetNumber;
    
    // must include all possible presets
    if (kPreset1 || kPreset2 || kPreset3 || kPreset4 || kPreset5 || kPreset6 || kPreset7 || kPreset8 || kPreset9) {
        for (int i=0; i<kPresetNum; i++) {
            if (choosenPreset == kPresets[i].presetNumber) {
                switch (choosenPreset) {
                    case kPreset1:
                        SetParameter(kParam_1, kParam1Default);
                        SetParameter(kParam_2, kParam2Default);
                        SetParameter(kParam_3, kParam3Default);
                        break;
                    case kPreset2:
                        SetParameter(kParam_1, 575.0);
                        SetParameter(kParam_2, 65.0);
                        SetParameter(kParam_3, kParam3TypeSine);
                        break;
                    case kPreset3:
                        SetParameter(kParam_1, 42.5);
                        SetParameter(kParam_2, 76.0);
                        SetParameter(kParam_3, kParam3TypeSawUp);
                        break;
                    case kPreset4:
                        SetParameter(kParam_1, 3672.0);
                        SetParameter(kParam_2, 72.0);
                        SetParameter(kParam_3, kParam3TypeTri);
                        break;
                    case kPreset5:
                        SetParameter(kParam_1, 3980.0);
                        SetParameter(kParam_2, 18.65);
                        SetParameter(kParam_3, kParam3TypeSquare);
                        break;
                    case kPreset6:
                        SetParameter(kParam_1, 530.0);
                        SetParameter(kParam_2, 84.4);
                        SetParameter(kParam_3, kParam3TypeSquare);
                        break;
                    case kPreset7:
                        SetParameter(kParam_1, 2345.7);
                        SetParameter(kParam_2, 48.3);
                        SetParameter(kParam_3, kParam3TypeSawDown);
                        break;
                    case kPreset8:
                        SetParameter(kParam_1, 4649.31);
                        SetParameter(kParam_2, 97.55);
                        SetParameter(kParam_3, kParam3TypeSawUp);
                        break;
                    case kPreset9:
                        SetParameter(kParam_1, 1300.0);
                        SetParameter(kParam_2, 35.5);
                        SetParameter(kParam_3, kParam3TypeSine);
                        break;
                }
                SetAFactoryPresetAsCurrent(kPresets[i]);
                return noErr;
            }
        }
    }
    return kAudioUnitErr_InvalidProperty;
}





#pragma mark - Private

bool MyTestedPluginKernal :: changedFreq(Float32 newFreq) {
    if (newFreq != osc->oscFreq()) return 1;
    else return 0;
}


bool MyTestedPluginKernal :: changedDepth(Float32 newDepth) {
    if (newDepth != osc->oscAmp()) return 1;
    else return 0;
}


bool MyTestedPluginKernal :: changedType(int newType) {
    if (newType != osc->oscType()) return 1;
    else return 0;
}






#pragma mark - Pre Process



OSStatus MyTestedPlugin::ProcessBufferLists(AudioUnitRenderActionFlags &ioActionFlags,
                                         const AudioBufferList         &inBuffer,
                                         AudioBufferList               &outBuffer,
                                         UInt32                        inFramesToProcess)
{
    // first check for silence and bypass if true
	bool ioSilence;
    bool silentInput = IsInputSilent (ioActionFlags, inFramesToProcess);
	ioActionFlags |= kAudioUnitRenderAction_OutputIsSilence;
    
    Float32 freq = GetParameter(kParam_1);
    
    if (inBuffer.mNumberBuffers == 1) {
        for (size_t i=0; i<mKernelList.size(); i++) {
            MyTestedPluginKernal *kernel = (MyTestedPluginKernal*) mKernelList[i];
            
            if (kernel == NULL) continue;
            ioSilence = silentInput;
            
            kernel->dspMono((const Float32*) inBuffer.mBuffers[0].mData + i,
                            (Float32*) outBuffer.mBuffers[0].mData + i,
                            inFramesToProcess,
                            ioSilence,
                            i,
                            freq);
            
            if (!ioSilence)
                ioActionFlags &= ~kAudioUnitRenderAction_OutputIsSilence;
        }
    }
    else {
        for (size_t i=0; i<mKernelList.size(); i++) {
            MyTestedPluginKernal *kernel = (MyTestedPluginKernal*) mKernelList[i];
            
            if (kernel == NULL) continue;
            ioSilence = silentInput;
            
            kernel->dspMono((const Float32*) inBuffer.mBuffers[i].mData,
                            (Float32*) outBuffer.mBuffers[i].mData,
                            inFramesToProcess,
                            ioSilence,
                            i,
                            freq);
            
            if (!ioSilence)
                ioActionFlags &= ~kAudioUnitRenderAction_OutputIsSilence;
        }
    }
    
    
    
    return noErr;
}







#pragma mark - Process




void MyTestedPluginKernal::dspMono (const Float32  *inSourceP,
                                    Float32        *inDestP,
                                    UInt32			inFramesToProcess,
                                    bool            &ioSilence,
                                    size_t          channel,
                                    Float32         freq)
{
    if (!ioSilence) {
        Float32         *inputBuffer = const_cast<Float32*>(inSourceP);
        
        Float32         depth = GetParameter(kParam_2) * 0.01;
        Float32         depthM1 = 1.0 - depth;
        UInt32          framesResampled = inFramesToProcess*kResampleFactor;
        
        int             waveType = static_cast<int>(GetParameter(kParam_3));
        
        // check for changed parameters
        if (changedFreq(freq)) {
            if (freq < kParam1Min) freq = kParam1Min;
            else if (freq > kParam1Max) freq = kParam1Max;
            osc->setFreq(static_cast<double>(freq));
        }
        
        if (changedDepth(depth)) {
            if (depth < kParam2Min) depth = kParam2Min;
            else if (depth > kParam2Max) depth = kParam2Max;
            osc->setAmp(static_cast<double>(depth));
            depthM1 = 1.0 - depth;
        }
        
        if (changedType(waveType)) osc->setType((OscType) waveType);
        
        
        // do upsampling
        sampleUp.process<Float32>(inputBuffer, buffUpSampleIn, inFramesToProcess);
        
        
        // Proc SSE
        aA_SSE_vScale(buffUpSampleIn, depthM1, buffUpSampleOut, framesResampled);
        osc->genSSE_Linear(oscBuff, framesResampled);
        aA_SSE_vMultiply(buffUpSampleIn, oscBuff, buffUpSampleIn, framesResampled);
        aA_SSE_vAdd(buffUpSampleIn, buffUpSampleOut, buffUpSampleOut, framesResampled);
        
//        // Proc Loop!!!!
//        for (int i=0; i < framesResampled; i++)
//            buffUpSample[i] += buffUpSample[i] * osc->genInterpLinear();
        
        
        // down sample
        sampleDn.process<Float32>(buffUpSampleOut, inDestP, inFramesToProcess);
    }
    // bypass
    else {
        *inDestP = *inSourceP;
        osc->setPhase(0.0);
    }
}









