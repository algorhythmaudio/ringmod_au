#ifndef RingMod_AUPlugin_h
#define RingMod_AUPlugin_h

#include <AudioToolbox/AudioUnitUtilities.h>
#include "AUEffectBase.h"

#include "AAOsc.h"
#include "AAResample.h"


#define kVERSION 0x0009151
#define kTableSize 11
#define kAllowedTailTime (Float64) 0.0 // default is 0


#define kHasCustomView 1 // make 1 for yes and 0 for no

using namespace AADsp;


class MyTestedPluginKernal : public AUKernelBase {

    
public:
    MyTestedPluginKernal                (AUEffectBase *inAudioUnit);
        
    // this method handles the dsp
    void                        dspMono     (const Float32  *inSourceP,
                                             Float32        *inDestP,
                                             UInt32			inFramesToProcess,
                                             bool           &ioSilence,
                                             size_t         channel,
                                             Float32        freq);

    virtual                     ~MyTestedPluginKernal();
    virtual void                Reset();
    
// ivars go here!
private:
    Float64                     sr;
    
    // osc
    AAOsc                       *osc;
    Float32                     *oscBuff = NULL;
    
    // Upsampling
    AAResample::Interpolation   sampleUp;
    AAResample::Decimation      sampleDn;

    Float32                     *buffUpSampleIn = NULL;
    Float32                     *buffUpSampleOut = NULL;
    
    // private methods
    bool                    changedFreq             (Float32 newFreq);
    bool                    changedDepth            (Float32 newDepth);
    bool                    changedType             (int newType);
};


class MyTestedPlugin : public AUEffectBase {

    
public:
    
    MyTestedPlugin                                      (AudioUnit component);
    virtual AUKernelBase    *NewKernel                  () {return new MyTestedPluginKernal(this);}

    virtual OSStatus        Version                     () {return kVERSION;}
    virtual OSStatus        Initialize();
    
    // parameter
    virtual OSStatus        GetParameterInfo           (AudioUnitScope			inScope,
                                                        AudioUnitParameterID   inParameterID,
                                                        AudioUnitParameterInfo	&outParameterInfo );
    
    virtual OSStatus        GetParameterValueStrings   (AudioUnitScope         inScope,
                                                        AudioUnitParameterID	inParameterID,
                                                        CFArrayRef *			outStrings);
    
    // property
    virtual OSStatus        GetPropertyInfo            (AudioUnitPropertyID    inID,
                                                        AudioUnitScope			inScope,
                                                        AudioUnitElement		inElement,
                                                        UInt32                 &outDataSize,
                                                        Boolean                &outWritable );

    virtual OSStatus        GetProperty                 (AudioUnitPropertyID 	inID,
                                                         AudioUnitScope 		inScope,
                                                         AudioUnitElement 		inElement,
                                                         void 					*outData );

    
    virtual OSStatus        GetPresets                  (CFArrayRef *outData) const;
    virtual OSStatus        NewFactoryPresetSet         (const AUPreset &inNewFactoryPreset);

    virtual	bool            SupportsTail                () { return true; } // this should always be true for effects
    virtual Float64         GetTailTime                 () {return kAllowedTailTime;}
    
    
    // This is the pre dsp proc function
    virtual OSStatus        ProcessBufferLists          (AudioUnitRenderActionFlags &ioActionFlags,
                                                         const AudioBufferList &inBuffer,
                                                         AudioBufferList &outBuffer,
                                                         UInt32 inFramesToProcess);

};




AUDIOCOMPONENT_ENTRY(AUBaseFactory, MyTestedPlugin);





// Presets
enum {
    kPreset1 = 0,
    kPreset2,
    kPreset3,
    kPreset4,
    kPreset5,
    kPreset6,
    kPreset7,
    kPreset8,
    kPreset9,
    kPresetNum
};

static AUPreset kPresets [kPresetNum] = {
    {kPreset1, CFSTR("Default")},
    {kPreset2, CFSTR("La Campanella")},
    {kPreset3, CFSTR("Lumberjack")},
    {kPreset4, CFSTR("The Golden Triangle")},
    {kPreset5, CFSTR("Buzz Lightyear")},
    {kPreset6, CFSTR("Harsh Distortion")},
    {kPreset7, CFSTR("Ice Scraper")},
    {kPreset8, CFSTR("Bad Circuitry")},
    {kPreset9, CFSTR("Halo")}
};

// factory default
static const int kPresetDefault = kPreset1;



#endif











