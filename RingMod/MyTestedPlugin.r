#include <AudioUnit/AudioUnit.r>

#define RES_ID          1000
#define COMP_TYPE       'aufx'
#define COMP_SUBTYPE    'RMod'
#define COMP_MANUF      'Algo'
#define VERSION         0x0009151
#define NAME            "AlgoRhythm Audio: The Ring"
#define DESCRIPTION     "A Basic ring modulator"
#define ENTRY_POINT     "MyTestedPluginEntry"

#include "AUResources.r"