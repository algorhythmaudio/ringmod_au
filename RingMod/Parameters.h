//
//  Parameters.h
//  RingMod
//
//  Created by GW on 9/30/12.
//
//

#ifndef RingMod_Parameters_h
#define RingMod_Parameters_h



#define kBundleName CFSTR("com.algorhythmaudio.RingMod")
#define kViewName CFSTR("RingModView")
#define kViewClass CFSTR("RingMod_CocoaViewFactory")

#define kResources


enum {
    kParam_1        = 0,
    kParam_2,
    kParam_3,
    kParamNum
};

enum {
    kParam3TypeSine = 0,
    kParam3TypeSawUp,
    kParam3TypeSawDown,
    kParam3TypeTri,
    kParam3TypeSquare,
    kParam3TypeNum
};


static CFStringRef  kParam1Name =  CFSTR("Frequency");
const float         kParam1Default = 2.0;
const float         kParam1Min     = 0.01;
const float         kParam1Max     = 5000.0;

static CFStringRef  kParam2Name = CFSTR("Depth");
const float         kParam2Default = 50.0;
const float         kParam2Min = 0.0;
const float         kParam2Max = 100.0;

static CFStringRef  kParam3Name = CFSTR("Wave Type"); // this is an indexed type
const int           kParam3Default = 0;
const int           kParam3Min = 0;
const int           kParam3Max = 4;
static CFStringRef  kParam3TypeSineName = CFSTR("Sine");
static CFStringRef  kParam3TypeSawUpName = CFSTR("Sawtooth Up");
static CFStringRef  kParam3TypeSawDownName = CFSTR("Sawtooth Down");
static CFStringRef  kParam3TypeTriName = CFSTR("Triangle");
static CFStringRef  kParam3TypeSquareName = CFSTR("Square");





#endif
