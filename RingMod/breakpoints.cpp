//
//  main.c
//  breakpoints
//
//  Created by GW Rodriguez on 3/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//


#include "breakpoints.h"
#include <stdlib.h>

#define NPOINTS 64
#define LINELENGTH 80

#ifndef MIN
#define MIN(x,y) ((x) < (y) ? (x) : (y))
#endif
#ifndef MAX
#define MAX(x,y) ((x) > (y) ? (x) : (y))
#endif




breakpoint maxpoint(const breakpoint* points,unsigned long npoints) {
	int i;
	breakpoint point;
    
	point.time = 0.0;
	point.value = 0.0;
    
	for(i=0; i<npoints; i++){
		if(point.value < points[i].value){
			point.value = points[i].value;
			point.time = points[i].time;
		}
	}			
	return point;
}

int bps_getminmax(brkstream *stream, double *outmin, double *outmax) {
	double val,minval,maxval;
	unsigned long i;
	
	if(stream==NULL || stream->npoints < 2)
		return -1;
	
	minval = maxval = stream->points[0].value;
    
	for(i=1;i < stream->npoints;i++){
		val = stream->points[i].value;
		minval = MIN(minval,val);
		maxval = MAX(maxval,val);
	}
	*outmin = minval;
	*outmax = maxval;
	return 0;
}



breakpoint *get_breakpoints(FILE *fp, unsigned long *psize) {
    int got;
    char line[LINELENGTH];
    
	long npoints = 0, size = NPOINTS;
	double lasttime    = 0.0;
	breakpoint *points = NULL;	
    
    if(fp == NULL) return NULL;
    
	points = (breakpoint*) malloc(sizeof(breakpoint) * size);
    if(points==NULL) return NULL;
    
    
	while(fgets(line,80,fp)){				
		if((got = sscanf(line, "%lf%lf", &points[npoints].time,&points[npoints].value) )<0)
			continue;			  /* empty line */
		if(got==0){
			printf("Line %ld has non-numeric data\n",npoints+1);
			break;
		}
		if(got==1){
			printf("Incomplete breakpoint found at point %ld\n",npoints+1);
			break;
		}		
		if(points[npoints].time < lasttime){
			printf("error in breakpoint data at point %ld: time not increasing\n", npoints+1);
			break;
		}
		
        lasttime = points[npoints].time;
		if(++npoints == size){
			breakpoint* tmp;
			
            size += NPOINTS;
			tmp = (breakpoint*) realloc(points, sizeof(breakpoint) * size);
			
            if(tmp == NULL)	{	/* too bad! */
				/* have to release the memory, and return NULL to caller */
				npoints = 0;
				free(points);
				points = NULL;
				break;		 
			}
			points = tmp;
		}			
	}
    
	if(npoints)	*psize = npoints;	
	return points;
}
 


int inrange(const breakpoint *points, double minval, double maxval, unsigned long npoints) {
    unsigned long i;
    int rangeOK = 1;
    
    for (i=0; i<npoints; i++) {
        if (points[i].value < minval || points[i].value > maxval) {
            rangeOK = 0;
            break;
        }
    }
    
    return rangeOK;
}




double valAtBrktime (const breakpoint *points, unsigned long npoints, double time) {
    unsigned long i;
    breakpoint left, right;
    double frac, val, width;
    
    // scan until we find a span containing out time
    for (i=1; i<npoints; i++)
        if (time <= points[i].time) break;
    
    // maintain final value if time beyond end of data
    if (i == npoints) return points[i-1].value;
    
    left = points[i-1];
    right = points[i];
    
    // check for instant jump - two points with same time
    width = right.time - left.time;
    if (width == 0.0) return right.value;
    
    // get value from this span using linear interp
    frac = (time - left.time) / width;
    val = left.value + ((right.value - left.value) * frac);
    
    return val;
}


brkstream *bps_newstream(FILE *fp, unsigned long srate, unsigned long *size) {
    brkstream *stream;
    breakpoint *points;
    unsigned long npoints;
    
    if (srate == 0) {
        puts("Error creating stream - srate cannot be zero\n");
        return NULL;
    }
    
    stream = (brkstream*) malloc(sizeof(brkstream));
    if (stream == NULL) return NULL;
    
    // load breakpoint file and setup stream info
    points = get_breakpoints(fp, &npoints);
    if (points == NULL){
        free(stream);
        return NULL;
    }
    
    if (npoints < 2) {
        puts("breakpoint file is too small - at least two points required");
        free(stream);
        return NULL;
    }
    
    // init the stream object
    stream->points = points;
    stream->npoints = npoints;
    stream->curpos = 0.0;
    stream->ileft = 0;
    stream->iright = 1;
    stream->incr = 1.0 / srate;
    
    // first span
    stream->leftpoint = stream->points[stream->ileft];
    stream->rightpoint = stream->points[stream->iright];
    stream->width = stream->rightpoint.time - stream->leftpoint.time;
    stream->height = stream->rightpoint.value - stream->leftpoint.value;
    stream->morePoints = 1;
    
    if (size)
        *size = stream->npoints;
    
    return stream;
}


double bps_tick(brkstream *stream) {
    double thisval, frac;
    
    // check end of brkdata
    if (stream->morePoints == 0)
        return stream->rightpoint.value;
    if (stream->width == 0.0)
        thisval = stream->rightpoint.value;
    else {
        // get val from this span using linear interp
        frac = (stream->curpos - stream->leftpoint.time) / stream->width;
        thisval = stream->leftpoint.value + (stream->height * frac);
    }
    
    // move up ready for next sample
    stream->curpos += stream->incr;
    if (stream->curpos > stream->rightpoint.time) {
        // need to goto next span?
        stream->ileft++;
        stream->iright++;
        
        if (stream->iright < stream->npoints) {
            stream->leftpoint = stream->points[stream->ileft];
            stream->rightpoint = stream->points[stream->iright];
            stream->width = stream->rightpoint.time - stream->leftpoint.time;
            stream->height = stream->rightpoint.value - stream->leftpoint.value;
        }
        else
            stream->morePoints = 0.0;
    }
    
    return thisval;
}


void bps_dealloc(brkstream *stream) {
    if (stream && stream->points) {
        free(stream->points);
        stream->points = NULL;
    }
}






















