//
//  breakpoints.h
//  breakpoints
//
//  Created by GW Rodriguez on 3/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef breakpoints_breakpoints_h
#define breakpoints_breakpoints_h



#include <stdio.h>




typedef struct breakpoint {
    double time;
    double value;
} breakpoint;

typedef struct breakpointStream {
    breakpoint      *points;
    breakpoint      leftpoint, rightpoint;
    unsigned long   npoints;
    unsigned long   ileft, iright;
    double          curpos;
    double          incr;
    double          width;
    double          height;
    int             morePoints;
}brkstream;



breakpoint maxpoint(const breakpoint* points,unsigned long npoints);
int bps_getminmax(brkstream *stream, double *outmin, double *outmax);

breakpoint *get_breakpoints(FILE *fp, unsigned long *psize);

int inrange(const breakpoint *points, double minval, double maxval, unsigned long npoints);


/* Linear interpolation
        I built this exact function in proj 14.  The only difference is that the order
        of the functions is a bit different, but basically the same
*/
double valAtBrktime (const breakpoint *points, unsigned long npoints, double time);

brkstream *bps_newstream(FILE *fp, unsigned long srate, unsigned long *size);

double bps_tick(brkstream *stream);

void bps_dealloc(brkstream *stream);






#endif






